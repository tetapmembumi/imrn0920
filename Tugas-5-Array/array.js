function range(startNum, finishNum){
	let x = [];
	if (startNum < finishNum) {
		for(let i=startNum; i<=finishNum; i++){
			x.push(i);	
		}
		//push startNum dengan ditambah 1
	}
	else if (startNum > finishNum) {
		for(let j=startNum; j>=finishNum; j--){
			x.push(j);
		}
		//push finishNum dikurang 1
	}
	else {
		x = -1;
	}
	return x;
}
console.log("Soal No 1");
console.log(range(1, 10)); //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)); // -1
console.log(range(11,18)); // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)); // [54, 53, 52, 51, 50]
console.log(range()); // -1
console.log("");

function rangeWithStep(startNum, finishNum, step){
	let y = [];
	if (startNum < finishNum) {
		for(let i=startNum; i<=finishNum; i+=step){
			y.push(i);
		}
	}
	else if (startNum > finishNum) {
		for(let i=startNum; i>=finishNum; i-=step){
			y.push(i);
		}
	}
	return y;
}
console.log("Soal No 2");
console.log(rangeWithStep(1, 10, 2));
console.log(rangeWithStep(11, 23, 3));
console.log(rangeWithStep(5, 2, 1));
console.log(rangeWithStep(29, 2, 4));
console.log("");

function sum(awal, akhir, deret){
	let total = 0;
	let y = [];
	if (awal < akhir) {
		for(let i=awal; i<=akhir; i+=deret){
			y.push(i);
		}
	}
	else if (awal > akhir) {
		for(let i=awal; i>=akhir; i-=deret){
			y.push(i);
		}
	}
	else if ((awal > akhir) && (deret == "")) {
		for(let i=awal; i>=akhir; i--){
			y.push(i);
		}
	}
	else if (deret == null) {
		rangeWithStep(awal);
	}

	for(let i=0; i<y.length; i++){
		total+=y[i];
	}
	return total;
}
console.log("Soal No 3");
console.log(sum(1,10)); // 55
console.log(sum(5, 50, 2)); // 621
console.log(sum(15,10)); // 75
console.log(sum(20, 10, 2)); // 90
console.log(sum(1)); // 1
console.log(sum()); // 0 
console.log("");

console.log("Soal No 4");
var input = [
                ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
                ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
                ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
                ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
            ];
// console.log(input.length);
// console.log(input[1].length);
function dataHandling(data){
	let hasil = "";
	for(let i=0; i<data.length; i++){
		for(let j=0; j<data[i].length; j++){
			if (j == 0) {
				hasil += "Nomor ID: " + input[i][j] + "\n";
			} 
			else if (j == 1){
				hasil += "Nama Lengkap: " + input[i][j] + "\n";
			} else if (j == 2){
				hasil += "TTL: " + input[i][j];
			} else if (j == 3){
				hasil += " " + input[i][j] + "\n";
			} else if (j == 4){
				hasil += "Hobi: " + input[i][j] + "\n\n";
			}
		}
	}
	return hasil;
}
console.log(dataHandling(input));

function balikKata(string){
	let hasilString = "";
	for(let i=string.length-1; i>=0; i--){
		hasilString+=string[i];
	}
	return hasilString;
}
console.log("Soal No 5");
console.log(balikKata("Kasur Rusak")); // kasuR rusaK
console.log(balikKata("SanberCode")); // edoCrebnaS
console.log(balikKata("Haji Ijah")); // hajI ijaH
console.log(balikKata("racecar")); // racecar
console.log(balikKata("I am Sanbers")); // srebnaS ma I 
console.log("");

console.log("Soal No 6");
var input = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];

function dataHandling2(input){
  	input.splice(1, 2)
  	input.splice(4, 1)
  	input.splice(1, 0, "Roman Alamsyah Elsharawy")
  	input.splice(2, 0, "Provinsi Bandar Lampung",)
  	input.splice(4, 1)
  	input.splice(4, 0, "Pria", "SMA Internasional Metro")
  
  	console.log(input)

  	date = input[3].split("/")
  	waktu = input[3].split("/")
  	bulan = parseInt(date[1])
  	switch (bulan){
    	case 1 : {
      		console.log("Januari")
      		break
    	}
    	case 2 : {
      		console.log("Februari")
      		break
    	}
    	case 3 : {
      		console.log("Maret")
      		break
    	}
    	case 4 : {
      		console.log("April")
      		break
    	}
    	case 5 : {
      		console.log("Mei")
      		break
    	}
    	case 6 : {
      		console.log("Juni")
      		break
    	}
    	case 7 : {
      		console.log("Juli")
      		break
    	}
    	case 8 : {
      		console.log("Agustus")
      		break
    	}
    	case 9 : {
      		console.log("September")
      		break
    	}
    	case 10 : {
      		console.log("Oktober")
      		break
    	}
    	case 11 : {
      		console.log("November")
      		break
    	}
    	case 12 : {
      		console.log("Desember")
    	  	break
    	}
  	}

  	sorting = date.sort(function (value1, value2) { 
  		return value2 - value1 
  	})
  	console.log(sorting)

  	tgl = waktu.join("-")
  	console.log(tgl)
  
  	nama = input[1].slice(0, 14)
	console.log(nama)
}

dataHandling2(input);