console.log("SOAL NO 1 LOOPING While");
var angka = 2;
console.log("LOOPING PERTAMA");
while(angka<=20){
	console.log(angka + " - I love coding");
	angka+=2;
}
var angka2 = 20;
console.log("LOOPING KEDUA");
while(angka2>=2){
	console.log(angka2 + " - I will become a mobile developer");
	angka2-=2;
}
console.log("\n");
console.log("SOAL NO 2 LOOPING menggunakan for");
for (var i = 1; i <= 20; i++) {
	if (i%2 == 0) {
		console.log(i + " - Berkualitas");
	}
	if (i%2 == 1) {
		if (i%3 == 0) {
			console.log(i + " - I love Coding");
		} else {
			console.log(i + " - Santai");
		}
	}
}
console.log("\n");
console.log("SOAL NO 3");
var x = 0;	
for(x = 0; x < 4; x++){
	console.log("########");
}

console.log("\n");
console.log("SOAL NO 4");
var n = 7;
for(var a=1; a<=n; a++){
	for(var b=1; b<=a; b++){
		process.stdout.write("#");
	}
	console.log("");
}

console.log("\n");
console.log("SOAL NO 5");
var z=7;
for (var x=0; x<z; x++) {
	for (var y=0; y<z; y++) {
		if (x%2 == 0 && y%2 == 1) {
            process.stdout.write("#")
        } else if (x%2 == 1 && y%2 == 0) {
            process.stdout.write("#")
        } else {
            process.stdout.write(" ")
        }
	}
	console.log("")
}