function arrayToObject(arr) {
  var now = new Date();
  var thisYear = now.getFullYear();
  for(var i=0; i<arr.length; i++){
    if(arr[i][3] != null && arr[i][3]<=thisYear){
      age = now.getFullYear() - arr[i][3]
    }
    else{
      age="Invalid Birthday";
    }
    var object = {
      firstName: arr[i][0],
      lastName: arr[i][1],
      gender: arr[i][2],
      age: age
    }
    console.log(i+1 + ". " + object.firstName + " " + object.lastName + " : ");
    console.log(object);
  }
}
 
// Driver Code
console.log("Soal No 1")
var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
arrayToObject(people) 

var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2)
console.log("")
console.log("Soal No 2")
function shoppingTime(memberId, money) {
  if(memberId == '' || memberId == null){
    return 'Mohon maaf, toko X hanya berlaku untuk member saja';
  }
  else if(money<50000){
    return 'Mohon maaf, uang tidak cukup'
  }
  else{
    var listPurchased = [];
    var changeMoney = money;

    if(changeMoney >= 1500000) {
      listPurchased.push("Sepatu Stacattu");
      changeMoney -= 1500000;
    }
    if(changeMoney >= 500000) {
      listPurchased.push("Baju Zoro");
      changeMoney -= 500000;
    }
    if(changeMoney >= 250000) {
      listPurchased.push("Baju H&N");
      changeMoney -= 250000;
    }
    if(changeMoney >= 175000) {
      listPurchased.push("Sweater Uniklooh");
      changeMoney -= 175000;
    }
    if(changeMoney >= 50000) {
      listPurchased.push("Casing Handphone");
      changeMoney -= 50000;
    }

    var object = {
      memberId: memberId,
      listPurchased: listPurchased,
      changeMoney: changeMoney
    };

    return object;
  }
}
console.log(shoppingTime('', 2475000));
console.log(shoppingTime('234JdhweRxa53', 15000));
console.log(shoppingTime('123JdhweRxa53', 2475000));
console.log(shoppingTime());
console.log("")

console.log("Soal No 3")
function naikAngkot(listPenumpang) {
    rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    hasil = [];
    
    for(i=0; i<listPenumpang.length; i++) {
        var start = false;
        var pass = 0;
        for(j=0;j<rute.length;j++) {
            if(start == false) {
                if(rute[j] == listPenumpang[i][1]) {
                    start = true;
                    pass++;
                }
            }
            else if(start == true && rute[j] != listPenumpang[i][2]) {
                pass++;
            }
            else if(rute[j] == listPenumpang[i][2]) {
                break;
            }
        }
        var jumlah = 2000 * pass;

        var object = {penumpang: listPenumpang[i][0],
            naikDari: listPenumpang[i][1],
            tujuan: listPenumpang[i][2],
            bayar: jumlah
        };

        hasil.push(object);

    }
    return hasil;
}

console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
 
console.log(naikAngkot([]));
