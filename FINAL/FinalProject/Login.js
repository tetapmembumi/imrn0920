import React, { useContext } from 'react'
import { StyleSheet, Text, View, TextInput, TouchableOpacity } from 'react-native';
import { UserContext } from './userContext';

const Login = ({navigation}) => {
    const {value, setValue } = useContext(UserContext)

    return (
        <View style={styles.container}>
            <View style={{marginBottom: 30}}>
                <Text style={{fontWeight: 'bold', fontSize: 30,}}>Welcome!</Text>
                <Text>Sign in to continue</Text>
            </View>
            <View style={styles.body}>
                <View style={{marginTop: 30}}>
                    <View style={{marginBottom: 20}}>
                        <Text style={{alignSelf: 'center'}}>Username</Text>
                        <TextInput
                            style={styles.textInput}
                            onChangeText={(text) => setValue(text)}
                            underlineColorAndroid='black'>
                        </TextInput>
                    </View>
                    <View style={{marginBottom: 20}}>
                        <Text style={{alignSelf: 'center'}}>Password</Text>
                        <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                            <TextInput style={styles.textInput}
                                underlineColorAndroid='black'>
                            </TextInput>
                        </View>
                    </View>
                    <View style={{flexDirection: 'column'}}>
                        <TouchableOpacity onPress={() => navigation.navigate('MainApp')} style={styles.signUpButton}>
                            <Text style={styles.signUpText}>Sign In</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        </View>
    )
}

export default Login

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginLeft: 20,
        marginTop: 20,
    },
    body: {
        height: 500,
        padding: 20,
        elevation: 2,
        borderWidth: 0.1,
        marginRight: 20,
        borderColor: 'grey',
        borderRadius: 10,
        alignItems: 'center'
    },
    textInput: {
        color: 'black',
        height: 40,
        width: 250,
    },
    signUpButton: {
        marginTop: 40,
        backgroundColor: 'black',
        marginRight: 15,
        height: 50,
        borderRadius: 5,
    },
    signUpText: {
        color: 'white',
        alignSelf: 'center',
        marginTop: 15,
        fontWeight: 'bold',
    },
    boxLogin: {
        flexDirection: 'row',
        borderWidth: 0.1,
        width: 130,
        alignItems: 'center',
        height: 50,
        borderRadius: 2,
    },
    iconlogin: {
        height: 20,
        width: 20,
        marginLeft: 15,
        borderRadius: 5,
    }
})
