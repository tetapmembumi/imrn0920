import React, { useContext } from 'react'
import { StyleSheet, Text, View, Image, FlatList, TouchableOpacity } from 'react-native'
import { UserContext } from './userContext';

// import MovieCard from './MovieCard'

const Home = ({navigation}) => {
    const {value, setValue } = useContext(UserContext)
    console.log(value)

    var movie = {
        title: 'Joker',
        image: 'https://m.media-amazon.com/images/M/MV5BNGVjNWI4ZGUtNzE0MS00YTJmLWE0ZDctN2ZiYTk2YmI3NTYyXkEyXkFqcGdeQXVyMTkxNjUyNQ@@._V1_SX300.jpg',
        id: 'tt7286456',
    }
    var movie2 = {
        title: 'BATTLE',
        image: 'https://m.media-amazon.com/images/M/MV5BOWUzMTkwYjEtZmEyZi00ZTU5LWE3ZTMtM2ZkYmE0ZjNhMjRhXkEyXkFqcGdeQXVyNTkzMzAwNzg@._V1_SX300.jpg',
        id: 'tt6854672',
    }
    var movie3 = {
        title: 'JOHN WICK',
        image: 'https://m.media-amazon.com/images/M/MV5BMTU2NjA1ODgzMF5BMl5BanBnXkFtZTgwMTM2MTI4MjE@._V1_SX300.jpg',
        id: 'tt2911666',
    }
    var movie4 = {
        title: 'LOGAN',
        image: 'https://m.media-amazon.com/images/M/MV5BYzc5MTU4N2EtYTkyMi00NjdhLTg3NWEtMTY4OTEyMzJhZTAzXkEyXkFqcGdeQXVyNjc1NTYyMjg@._V1_SX300.jpg',
        id: 'tt3315342',
    }
    var movie5 = {
        title: 'RAID',
        image: 'https://m.media-amazon.com/images/M/MV5BN2NlMmUyZWUtZmI5Yy00YWM3LTkxYzgtM2ZiOTMwNTc5ZDg0XkEyXkFqcGdeQXVyNjcyNjMzMjQ@._V1_SX300.jpg',
        id: 'tt7363076',
    }
    var movie6 = {
        title: 'Laskar Pelangi',
        image: 'https://m.media-amazon.com/images/M/MV5BMDc5ZTBkNjYtMzk0OS00MDQwLTgyMWUtMWU2YzhmNjJmNjIxXkEyXkFqcGdeQXVyNjQ4ODY4NzU@._V1_SX300.jpg',
        id: 'tt1301264',
    }
    var movie7 = {
        title: 'Power Rangers',
        image: 'https://m.media-amazon.com/images/M/MV5BMTU1MTkxNzc5NF5BMl5BanBnXkFtZTgwOTM2Mzk3MTI@._V1_SX300.jpg',
        id: 'tt3717490',
    }
    var movie8 = {
        title: 'Detective Story',
        image: 'https://m.media-amazon.com/images/M/MV5BMTkxYzQ0MzgtMzUyOS00OGYyLTg0MDAtODY5MTM1NmQwMmZmXkEyXkFqcGdeQXVyMDI2NDg0NQ@@._V1_SX300.jpg',
        id: 'tt0043465',
    }

    var movie9 = {
        title: 'Now You See Me',
        image: 'https://m.media-amazon.com/images/M/MV5BMTY0NDY3MDMxN15BMl5BanBnXkFtZTcwOTM5NzMzOQ@@._V1_SX300.jpg',
        id: 'tt1670345',
    }

    var movie10 = {
        title: 'The Avengers',
        image: 'https://m.media-amazon.com/images/M/MV5BNDYxNjQyMjAtNTdiOS00NGYwLWFmNTAtNThmYjU5ZGI2YTI1XkEyXkFqcGdeQXVyMTMxODk2OTU@._V1_SX300.jpg',
        id: 'tt0848228',
    }
    
    let movieArr = []
    movieArr.push(movie)
    movieArr.push(movie2)
    movieArr.push(movie3)
    movieArr.push(movie4)
    movieArr.push(movie5)
    movieArr.push(movie6)
    movieArr.push(movie7)
    movieArr.push(movie8)
    movieArr.push(movie9)
    movieArr.push(movie10)
    

    const _movieCard = ({item, index}) => {
        return (
            <View style={styles.MovieCardBox}>
                <TouchableOpacity style={styles.Button} onPress={() => navigation.push('Detail', { movieId: item.id})}>
                <Image style={styles.MovieImage} source={{uri: item.image}}/>
                </TouchableOpacity>
            </View>
        )
    }

    return (
        <FlatList
            data={movieArr}
            numColumns={2}
            renderItem={this, _movieCard}
            keyExtractor = { (item, index) => index.toString()}
        />
    )
}

export default Home

const styles = StyleSheet.create({
    MovieImage: {
        width: '100%',
        height: '100%',
    },
    MovieCardBox: {
        marginTop: 4,
        marginLeft: 15,
        width: '43%',
        height: 220,
        borderRadius: 10,
        marginBottom: 5,
    },
    Button: {
        height: '100%',
        width: '100%',
    },
});