import React from 'react'
import { View, Text, StyleSheet, Image, TouchableOpacity } from 'react-native'

export default class MovieCard extends React.Component {
    render() {
        let mc = this.props.mc
        return (
            <View style={styles.MovieCardBox}>
                <TouchableOpacity style={styles.Button} onPress={() => navigation.navigate('Detail'), { name: "React Native by Example " }}>
                <Image style={styles.MovieImage} source={{uri: mc.image}}/>
                <Text numberOfLines={1} style={{marginLeft: 5, marginTop: 5, fontWeight: 'bold', alignSelf: 'center'}}>{mc.title}</Text>
                </TouchableOpacity>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    MovieImage: {
        width: '100%',
        height: '80%',
    },
    MovieCardBox: {
        marginTop: 2,
        marginLeft: 15,
        width: '43%',
        height: 300,
    },
    Button: {
        height: '100%',
        width: '100%',
    },
});