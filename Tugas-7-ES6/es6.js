goldenFunction = () => {
	console.log("this is golden!!")
}
console.log("Soal No 1");
goldenFunction()
console.log("")

console.log("Soal No 2")
const newFunction = function literal(firstName, lastName){
	return {
    	firstName: firstName,
    	lastName: lastName,
    	fullName : () => {
    		console.log(firstName + " " + lastName)
    		return
    	}
	}
}
newFunction("William", "Imoh").fullName()
console.log()

console.log("Soal No 3");
const newObject = {
  	firstName: "Harry",
  	lastName: "Potter Holt",
  	destination: "Hogwarts React Conf",
	occupation: "Deve-wizard Avocado",
	spell: "Vimulus Renderus!!!"
}
const{firstName, lastName, destination, occupation, spell} = newObject;
console.log(firstName, lastName, destination, occupation);
console.log()

console.log("Soal No 4")
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combine = [...west, ...east]
console.log(combine)
console.log("")

console.log("Soal No 5")
const planet = "earth"
const view = "glass"
var before = `Lorem ${view} dolor sit amet consectetur adipiscing elit ${planet} do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam`

console.log(before);