class Score {
  constructor(points) {
    this.email;
    this.subjects;
    this.points = points
  }

  average() {
    var avg;
    if (Array.isArray(this.points)) {
      var jlh = 0;
      for (var i = 0; i < this.points.length; i++) {
        jlh += parseInt(this.points[i], 10);
      }
      avg = jlh / this.points.length;
    } else if (this.points == 'number') {
      avg = this.points;
    }
    return Number((avg).toFixed(1));
  }
}

const ScoreClass = new Score([10,2,9]);
const ScoreClass2 = new Score([1])
console.log('Soal No 1');
console.log(ScoreClass.average())
console.log(ScoreClass2.average())
console.log()

console.log('Soal No 2');
const data = 
  [
    ["email", "quiz - 1", "quiz - 2", "quiz - 3"],
    ["abduh@mail.com", 78, 89, 90],
    ["khairun@mail.com", 95, 85, 88],
    ["bondra@mail.com", 70, 75, 78],
    ["regi@mail.com", 91, 89, 93]
  ]
subject =  "quiz-1"

viewScores = (data, subject) => {
  var x = 0
  var flag = 0

  if(subject[5] == data[0][1][7]){
    for ( x = 1; x < data.length; x++){
      console.log(`email: ${data[x][0]}` + '\n' + `subject: ${subject}` + '\n' + `points: ${data[x][1]}`)
    }
  }
  else if(subject[5] == data[0][2][7]){
    for ( x = 1; x < data.length; x++){
      console.log(`email: ${data[x][0]}` + '\n' + `subject: ${subject}` + '\n' + `points: ${data[x][1]}`)
    }
  }
  else if(subject[5] == data[0][3][7]){
    for ( x = 1; x < data.length; x++){
      console.log(`email: ${data[x][0]}` + '\n' + `subject: ${subject}` + '\n' + `points: ${data[x][1]}`)
    }
  }
}

viewScores(data, "quiz-1")
viewScores(data, "quiz-2")
viewScores(data, "quiz-3")
console.log()
console.log('Soal No 3')

function recapScores(data) {
  for(i=0;i<data.length;i++) {
    var avg = 0;
    for(j=1;j<data[i].length;j++) {      
      avg += data[i][j];
    }
    avg = avg/(data[i].length-1);  
    console.log(i+1+ `. Email: ${data[i][0]} \nRata-rata: ${avg}`);
    
    if(avg>70) {
      console.log("Predikat: participant");
    }
    else if(avg>80) {
      console.log("Predikat: graduate");
    }
    else if(avg>90) {
      console.log("Predikat: honour");
    }
  }
}
  
recapScores(data);